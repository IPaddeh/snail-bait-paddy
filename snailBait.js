//Snail Bait
var SnailBait = function()
{
    this.canvas = document.getElementById('game-canvas'),
    this.context = this.canvas.getContext('2d'),
    this.fpsElement = document.getElementById('fps'),
    this.toastElement = document.getElementById('toast'),
    this.instructionsElement = document.getElementById('instructions'),
    this.copyrightElement = document.getElementById('copyright'),
    this.soundAndMusicElement = document.getElementById('sound-and-music'),
    //Constants
    this.PLATFORM_HEIGHT = 8,
    this.PLATFORM_STROKE_WIDTH = 2,
    this.PLATFORM_STROKE_STYLE = 'rgb(0,0,0)', //black

    this.RUNNER_LEFT = 50,
    this.STARTING_RUNNER_TRACK = 1,
    this.TRANSACTION = 0,
    this.OPAQUE =1,
    this.SHORT_DELAY = 50;
   
    
    
    //Track Baselines
    this.TRACK_1_BASELINE = 323,
    this.TRACK_2_BASELINE = 223,
    this.TRACK_3_BASELINE = 123,
    //Scrolling Constants
    this.STARTING_BACKGROUND_VELOCITY = 25,
    this.STARTING_BACKGROUND_OFFSET = 0,
    this.STARTING_PLATFORM_OFFSET = 0,
    this.PLATFORM_VELOCITY_MULTIPLIER = 4.25,
    //Images
    this.background = new Image(),
    this.runnerImage = new Image(),

    //States
    this.paused = false,
    this.PAUSED_CHECK_INTERVAL = 200,

    //Time  
    this.lastAnimationFrameTime = 0,
    this.fps = 60,
    this.bgVelocity = this.STARTING_BACKGROUND_VELOCITY,
    this.pVelocity = this.STARTING_PLATFORM_OFFSET,
    this.lastFpsUpdateTime = 0,
    this.pausedStartTime,
    //Translation offsets
    this.backgroundOffset = this.STARTING_BACKGROUND_OFFSET,
    this.platformOffset = this.STARTING_PLATFORM_OFFSET,

    //Platforms
    this.platformData = [
    //Screeen One (0-800)
    {
        left        : 10,
        width       : 230,
        fillstyle   : 'rgb(250,250,0)',
        opacity     : 1.0,
        track       : 1,
        pulsate     : false,
    },
    {
        left        : 250,
        width       : 100,
        fillstyle   : 'rgb(150,190,255)',
        opacity     : 1.0,
        track       : 2,
        pulsate     : false,
    },
    {
        left        : 400,
        width       : 125,
        fillstyle   : 'rgb(250,0,0)',
        opacity     : 1.0,
        track       : 3,
        pulsate     : false,
    },
    {
        left        : 633,
        width       : 100,
        fillstyle   : 'rgb(250,250,0)',
        opacity     : 1.0,
        track       : 1,
        pulsate     : false,
    },
    //Screeen Two (800-1600)
    {
        left        : 810,
        width       : 100,
        fillstyle   : 'rgb(250,125,0)',
        opacity     : 1.0,
        track       : 3,
        pulsate     : false,
    },
    {
        left        : 1025,
        width       : 230,
        fillstyle   : 'rgb(150,200,225)',
        opacity     : 1.0,
        track       : 2,
        pulsate     : false,
    },
    {
        left        : 1220,
        width       : 100,
        fillstyle   : 'rgb(250,0,0)',
        opacity     : 1.0,
        track       : 3,
        pulsate     : false,
    },
    {
        left: 1400,
        width: 150,
        fillstyle: 'rgb(0,250,0)',
        opacity: 1.0,
        track: 1,
        pulsate: false,
    },
    //Screen Three (1600-2400
    {
        left: 1625,
        width: 100,
        fillstyle: 'rgb(200,250,0)',
        opacity: 1.0,
        track: 1,
        pulsate: false,
    },
    {
        left: 1800,
        width: 250,
        fillstyle: 'rgb(200,190,255)',
        opacity: 1.0,
        track: 1,
        pulsate: false,
    },
    {
        left: 2000,
        width: 100,
        fillstyle: 'rgb(250,154,0)',
        opacity: 1.0,
        track: 3,
        pulsate: false,
    },
    {
        left: 2250,
        width: 100,
        fillstyle: 'rgb(250,100,100)',
        opacity: 1.0,
        track: 3,
    },
    //Screen Four (2400-3200)
    {
        left: 2269,
        width: 200,
        fillstyle: 'gold',
        opacity: 1.0,
        track: 1,
    },
    {
        left: 2500,
        width: 200,
        fillstyle: 'rgb(150,190,255)',
        opacity: 1.0,
        track: 2,
        snail: true,
    },
    ];
};

//Launch the game
SnailBait.prototype = {

    initializeImages: function()
    {
        this.background.src = 'images/background.png';
        this.runnerImage.src = 'images/runner.png';

        this.background.onload = function (e)
        {
            snailBait.startGame();
        };
    },

    startGame: function()
    {
        window.requestAnimationFrame(this.animate);
    },

    animate: function(now)
    {   
    	if (snailBait.paused) 
    	{
    		setTimeout(function () 
    		{
    			requestAnimationFrame(snailBait.animate);
    		}, snailBait.PAUSED_CHECK_INTERVAL);	
    	} 
    	else 
    	{
    		snailBait.fps = snailBait.calculateFps(now);
        	snailBait.draw(now);
        	snailBait.lastAnimationFrameTime = now;
        	requestAnimationFrame(snailBait.animate);
    	}  
    },

    draw: function(now)
    {
        this.setPlatformVelocity();
        this.setOffSets(now);
        this.drawBackground();
        this.drawPlatforms();
        this.drawRunner();
        
    },

    setPlatformVelocity: function()
    {
        this.pVelocity = this.bgVelocity * this.PLATFORM_VELOCITY_MULTIPLIER;
    },

    setOffSets: function(now)
    {
        this.setBackgroundOffset(now);
        this.setPlatformOffset(now);
    },

    calculateFps: function(now)
    {
        var fps = 1/(now - this.lastAnimationFrameTime) * 1000;
        if(now - this.lastFpsUpdateTime > 1000)
        {
            this.lastFpsUpdateTime = now;
            this.fpsElement.innerHTML = fps.toFixed(0) + 'fps';
        }
        return fps;
    },

    setBackgroundOffset: function(now)
    {
        this.backgroundOffset += this.bgVelocity*(now-this.lastAnimationFrameTime)/1000;
        if(this.backgroundOffset<0 || this.backgroundOffset > this.background.width)
        {
            this.bgVelocity =this.bgVelocity*(-1);
        }
    },

    setPlatformOffset: function(now)
    {
        this.platformOffset += this.pVelocity*(now-this.lastAnimationFrameTime)/1000;
        if(this.platformOffset > 2*(this.background.width))
        {
            this.turnLeft()
        }
        else if (this.platformOffset < 0)
        {
            this.turnRight();
        }
    },

    turnLeft: function()
    {
        this.bgVelocity = -this.STARTING_BACKGROUND_VELOCITY;
    },

    turnRight: function()
    {
        this.bgVelocity= this.STARTING_BACKGROUND_VELOCITY;
    },

    togglePaused: function()
    {
    	var now = +new Date();
    	this.paused = !this.paused;
    	if (this.paused)
    	{
    		this.pausedStartTime = now;
    	}
    	else
    	{
    		this.lastAnimationFrameTime += (now - this.pausedStartTime);
    	}
    },
    
    revealToast: function(text, duration)
    {
        var DEFAULT_TOAST_DISPLAY = 1000;
        duration = duration || DEFAULT_TOAST_DISPLAY;
        this.toastElement.style.display = 'block';
        this.toastElement.innerHTML = text;
        
        setTimeout( function (e){
            if(snailBait.windowHasFocus)
            {
                snailBait.toastElement.style.display = 'none';
            }
        }, duration);
    },
    
    fadeInElements: function()
    {
        var args = arguments;
        for(var i = 0; i <args.length; i++)
        {
            args[i].style.display = 'block';
        }
        
        setTimeout(function()
        {
            for(var i = 0; i < args.length; i++)
            {
                args[i].style.opacity = snailBait.OPAQUE;
            }
        }, this.SHORT_DELAY);
    },
    
     fadeOutElements: function()
    {
        var args = arguments;
        fadeDuration = args[args.length-1];
        for(var i = 0; i <args.length; i++)
        {
            args[i].style.opacity = snailBait.TRANSACTION;
        }
        
        setTimeout(function()
        {
            for(var i = 0; i < args.length; i++)
            {
                args[i].style.opacity = 'none';
            }
        }, this.fadeDuration);
    },
    
    hideToast: function()
    {
        var TOAST_TRANSITION_DURATION = 450;
        this.fadeOutElements(this.toastElement, TOAST_TRANSITION_DURATION)
    },

    drawBackground: function()
    {
        this.context.translate(-this.backgroundOffset,0);
        //Initially Onscreen
        this.context.drawImage(this.background, 0, 0);
        //Initially Offscreen
        this.context.drawImage(this.background, this.background.width, 0);
        this.context.translate(this.backgroundOffset, 0);
    },

    drawPlatforms: function()
    {
        this.context.translate(-this.platformOffset,0);
        //Initially Onscreen
        for (var i = 0; i < this.platformData.length; ++i)
        {
            this.drawPlatform(this.platformData[i]);
        }
        this.context.translate(this.platformOffset, 0);
    },

    drawPlatform: function(data)
    {
        var platformTop = this.calculatePlatformTop(data.track);
        this.context.lineWidth = this.PLATFORM_STROKE_WIDTH;
        this.context.strokeStyle = this.PLATFORM_STROKE_STYLE;
        this.context.fillStyle = data.fillstyle;
        this.context.globalAlpha = data.opacity;

        this.context.strokeRect(data.left, platformTop, data.width, this.PLATFORM_HEIGHT);
        this.context.fillRect(data.left, platformTop, data.width, this.PLATFORM_HEIGHT);
    },

    calculatePlatformTop: function(track)
    {
        if (track === 1)
        {
            return this.TRACK_1_BASELINE;
        }
        else if (track === 2)
        {
            return this.TRACK_2_BASELINE;
        }
        else if (track === 3)
        {
            return this.TRACK_3_BASELINE;
        }
    },

    drawRunner: function()
    {
        this.context.drawImage(this.runnerImage, this.RUNNER_LEFT, this.calculatePlatformTop(this.STARTING_RUNNER_TRACK) - this.runnerImage.height);
    }

};

//add some vars for keycodes
var KEY_D = 68,
	KEY_LEFT_ARROW = 37,
	KEY_RIGHT_ARROW = 39,
	KEY_K = 75,
	KEY_P = 80;
//Add Event Listener 
window.addEventListener('keydown', function (e)
{
	var key = e.keyCode;
	if (key === KEY_D || key === KEY_LEFT_ARROW)
	{
		snailBait.turnLeft();
	}
	else if (key === KEY_K || key === KEY_RIGHT_ARROW)
	{
		snailBait.turnRight();
	}
	else if(key === KEY_P)
	{
		snailBait.togglePaused();
	}
}),

window.addEventListener('blur', function (e){
    snailBait.windowHasFocus = false;
    if(!snailBait.paused)
    {
        snailBait.togglePaused();
    }
}),
        
window.addEventListener('focus', function (e)
{
    var originalFont = snailBait.toastElement.style.fontSize;
    DIGIT_DISPLAY_DURATION = 1000; //milliseconds
    snailBait.windowHasFocus = true;
    snailBait.countdownInProgress = true;
    if(snailBait.paused)
    {
        snailBait.toastElement.style.font = '128px fantasy';
        if(snailBait.windowHasFocus && snailBait.countdownInProgress)
        {
            snailBait.revealToast('3', DIGIT_DISPLAY_DURATION);
            setTimeout(function (e)
            {
               if(snailBait.windowHasFocus && snailBait.countdownInProgress)
                    snailBait.revealToast('2', DIGIT_DISPLAY_DURATION);
            
                setTimeout(function (e)
                {
                   if(snailBait.windowHasFocus && snailBait.countdownInProgress)
                        snailBait.revealToast('1', DIGIT_DISPLAY_DURATION);

                    setTimeout(function (e)
                    {
                       if(snailBait.windowHasFocus && snailBait.countdownInProgress)
                            snailBait.togglePaused();
                        snailBait.toastElement.style.font = originalFont;
                        snailBait.countdownInProgress = false;
                    }, DIGIT_DISPLAY_DURATION);
                }, DIGIT_DISPLAY_DURATION);
            }, DIGIT_DISPLAY_DURATION);
        }
    }
});

var snailBait = new SnailBait();
snailBait.initializeImages();